# Why AMP (Accelerated Mobile Pages) for TYPO3?

## The AMP Extension provides accelerated mobile pages for TYPO3.

Accelerated mobile pages (short: AMP) are "load-optimized" webpages, specifically designed for mobile devices like smartphones or tablets.

**The advantages at a glance:**

*   AMP pages load faster than classical web-pages and make digital content more accessible to your users.
*   AMP sites are better served on mobile devices by Google and thus listed above in the search function. You can discern these AMP pages in Google's result by a small flash icon beside.
*   AMP pages are already loaded in the Google search results in the "top stories list".
*   Create customized and top-of-the-line AMP views of your news, blog, shop, reference or real estate listings and supply them immediately to your customers.
*   For this the TYPO3-AMP basic version provides you with additionally TYPO3 page type that generates the AMP pages.

Read [here](https://www.ampproject.org/learn/overview/#video "AMP Project - How AMP works in detail") how AMP works in detail.
