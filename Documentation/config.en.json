{
    "title": "TYPO3 - AMP",
    "subtitle": "Accelerated mobile pages (short: AMP) are \"load-optimized\" webpages, specifically designed for mobile devices like smartphones or tablets.",
    "logo": "../Resources/Public/Icons/Extension.svg",
    "markdown": "../Readme.en.md",
    "features": [
        "Basic functionality for AMP pages in TYPO3.",
        "Flexible configuration with Setup-TypoScript.",
        "Easy styling with Fluid and CSS",
        "Option to track AMP pages using Google Analytics",
        "AMP functionality for news posts, e.g. from the tt_news or Georg Ringer extensions.",
        "AMP functionality for property portfolios displayed in the TYPO3 OpenImmo extension",
        "AMP functionality for our shop extension - all products can now be accessed via AMP",
        "Other page types such as testimonials, blog entries and much more can easily be integrated - contact us!"
    ],
    "meta": {
        "description": "TYPO3 - AMP - Accelerated mobile pages (short: AMP) are \"load-optimized\" webpages, specifically designed for mobile devices like smartphones or tablets.",
        "abstract": "TYPO3 - AMP",
        "keywords": "TYPO3, extension, AMP",
        "og:title": "TYPO3 - AMP",
        "og:description": "TYPO3 - AMP - Accelerated mobile pages (short: AMP) are \"load-optimized\" webpages, specifically designed for mobile devices like smartphones or tablets."
    },
    "links": {
        "website": {
            "link": "https://www.typo3-amp.de",
            "name": "TYPO3 AMP Website"
        },
        "product": {
            "link": "https://www.coding.ms/typo3-extensions/typo3-amp/",
            "name": "TYPO3 AMP Productdetails"
        },
        "amp": {
            "link": "https://www.ampproject.org/",
            "name": "AMP Project Website"
        },
        "amp-by-example": {
            "link": "https://amp.dev/documentation/examples/",
            "name": "AMP By Example"
        },
        "amp-validator": {
            "link": "https://validator.ampproject.org/",
            "name": "AMP Validator"
        }
    },
    "pages": [
        {
            "title": "Why AMP for TYPO3",
            "markdown": "Why.en.md",
            "meta": {
                "description": "TYPO3 AMP - Why AMP for TYPO3?",
                "abstract": "Good reasons to use our TYPO3 AMP Extension",
                "keywords": "TYPO3, Extension, AMP, why",
                "og:title": "TYPO3 AMP - Why AMP for TYPO3?",
                "og:description": "Good reasons to use our TYPO3 AMP Extension and accelerate your website"
            }
        },
        {
            "title": "Configuration",
            "pages": [
                {
                    "title": "Integration",
                    "markdown": "Configuration/Integration.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Easy integration by just adding the appropriate static template on your detail page.",
                        "abstract": "Integration and configuration of TYPO3 AMP",
                        "keywords": "TYPO3, Extension, AMP, integration",
                        "og:title": "TYPO3 AMP - Integration",
                        "og:description": "Easy integration by just adding the appropriate static template on your detail page."
                    }
                },
                {
                    "title": "Human readable URLs",
                    "markdown": "Configuration/HumanReadableUrls.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Set up human readable urls in TYPO3 AMP extension and configure slugs",
                        "abstract": "Set up human readable URLs for TYPO3 AMP extension",
                        "keywords": "TYPO3, Extension, AMP, url, human readable",
                        "og:title": "TYPO3 AMP - Human readable URLs",
                        "og:description": "Set up human readable urls in TYPO3 AMP extension and configure slugs"
                    }
                },
                {
                    "title": "Styling the AMP view",
                    "markdown": "Configuration/Styling.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Styling the AMP view. The AMP extension comes with a small static template that contains some basic styles.",
                        "abstract": "Configure the view styling of TYPO3 AMP extension",
                        "keywords": "TYPO3, Extension, AMP, url, view, configuration",
                        "og:title": "TYPO3 AMP - Styling the AMP view",
                        "og:description": "Styling the AMP view. The AMP extension comes with a small static template that contains some basic styles."
                    }
                },
                {
                    "title": "Contact form",
                    "markdown": "Configuration/ContactForm.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Add a contact form to TYPO3 AMP extension, if you use the pro version",
                        "abstract": "Add a contact form to TYPO3 AMP Pro extension",
                        "keywords": "TYPO3, Extension, AMP, contact, form",
                        "og:title": "TYPO3 AMP - Contact form",
                        "og:description": "Add a contact form to TYPO3 AMP extension, if you use the pro version"
                    }
                },
                {
                    "title": "Templating",
                    "markdown": "Configuration/Templating.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Configuration of templates in TYPO3 AMP. The AMP extension uses simple Fluid templates to render the AMP pages.",
                        "abstract": "Configuration of templates in TYPO3 AMP",
                        "keywords": "TYPO3, Extension, AMP, templates, configuration",
                        "og:title": "TYPO3 AMP - Templating",
                        "og:description": "Configuration of templates in TYPO3 AMP. The AMP extension uses simple Fluid templates to render the AMP pages."
                    }
                },
                {
                    "title": "TYPO3-Extensions",
                    "markdown": "Configuration/Typo3Extensions.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Supported TYPO3-Extensions",
                        "abstract": "Supported TYPO3-Extensions of TYPO3 AMP",
                        "keywords": "TYPO3, Extension, supported",
                        "og:title": "TYPO3 AMP - Supported TYPO3-Extensions",
                        "og:description": "Supported TYPO3-Extensions of TYPO3 AMP"
                    }
                },
                {
                    "title": "AMP-Libraries",
                    "markdown": "Configuration/AmpLibraries.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - AMP-Libraries",
                        "abstract": "Supported TYPO3 AMP libraries",
                        "keywords": "TYPO3, Extension, AMP, library, libraries",
                        "og:title": "TYPO3 AMP - AMP-Libraries",
                        "og:description": "Supported TYPO3 AMP libraries"
                    }
                }
            ]
        },
        {
            "title": "HowTo",
            "pages": [
                {
                    "title": "Show News Images with Content Elements",
                    "markdown": "HowTo/DisplayImagesFromNewsContentElements.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Show News Images with Content Elements",
                        "abstract": "How to show news images with content elements in TYPO3 AMP by editing the Fluid Partial.",
                        "keywords": "TYPO3, Extension, AMP, images, news",
                        "og:title": "TYPO3 AMP - Show News Images with Content Elements",
                        "og:description": "How to show news images with content elements in TYPO3 AMP by editing the Fluid Partial."
                    }
                },
                {
                    "title": "Insert Google Analytics tracking",
                    "markdown": "HowTo/InsertGoogleAnalyticsTracking.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Insert Google Analytics tracking",
                        "abstract": "How to insert Google Analytics tracking in TYPO3 AMP",
                        "keywords": "TYPO3, Extension, AMP, google, analytics, tracking",
                        "og:title": "TYPO3 AMP - Insert Google Analytics tracking",
                        "og:description": "How to insert Google Analytics tracking in TYPO3 AMP"
                    }
                }
            ]
        },
        {
            "title": "FAQ",
            "pages": [
                {
                    "title": "The AMP-Form doesn't send a mail - what to do?",
                    "markdown": "Faq/FormDoesntSendMail.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - Configuration and settings when the form stops sending email.",
                        "abstract": "Configuration and settings when the form stops sending email",
                        "keywords": "TYPO3, extension, Amp, email, form",
                        "og:title": "TYPO3 AMP - The AMP-Form doesn't send a mail - what to do?",
                        "og:description": "Configuration and settings when the form stops sending email."
                    }
                },
                {
                    "title": "I want to disable the AMP Header-Tag while developing the design, but need the AMP-Link - how can I solve this  problem?",
                    "markdown": "Faq/DisableAmpHeaderTag.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to solve the problem when you want to disable AMP header tag during development but need AMP link.",
                        "abstract": "Disable AMP header tag during development",
                        "keywords": "TYPO3, extension, AMP, Header, Link",
                        "og:title": "TYPO3 AMP - I want to disable the AMP header tag while developing the theme, but I need the AMP link. How can I solve this problem?",
                        "og:description": "How to solve the problem when you want to disable AMP header tag during development but need AMP link."
                    }
                },
                {
                    "title": "Can I disable the AMP view?",
                    "markdown": "Faq/DisableAmpView.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to disable AMP view using TypoScript.",
                        "abstract": "Disable AMP view",
                        "keywords": "TYPO3, extension, AMP, view",
                        "og:title": "TYPO3 AMP - Can I disable the AMP view?",
                        "og:description": "How to disable AMP view using TypoScript."
                    }
                },
                {
                    "title": "I receive an error when sending the AMP-Form - how do I fix it?",
                    "markdown": "Faq/ErrorAmpForm.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to troubleshoot when you receive an error message while submitting AMP form.",
                        "abstract": "Error message when submitting a form",
                        "keywords": "TYPO3, extension, AMP, error, message, send, form",
                        "og:title": "TYPO3 AMP - I receive an error when sending the AMP-Form - how do I fix it?",
                        "og:description": "How to troubleshoot when you receive an error message while submitting AMP form"
                    }
                },
                {
                    "title": "How can I switch the URLs to HTTPS/SSL?",
                    "markdown": "Faq/SwitchUrls.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to switch to HTTPS/SSL with a TypoScript constant.",
                        "abstract": "Switch to HTTPS/SSL with a TypoScript constantn",
                        "keywords": "TYPO3, extension, AMP, typoscript, http, ssl",
                        "og:title": "TYPO3 AMP - How can I switch the URLs to HTTPS/SSL?",
                        "og:description": "How to switch to HTTPS/SSL with a TypoScript constant."
                    }
                },
                {
                    "title": "How can I translate/modify the localization labels?",
                    "markdown": "Faq/TranslateLocalizationLabels.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to easily translate or change the localization identifier with the help of TypoScript.",
                        "abstract": "Lokalisierungskennung übersetzen oder ändern",
                        "keywords": "TYPO3, extension, AMP, localization, label",
                        "og:title": "TYPO3 AMP - How can I translate/modify the localization labels?",
                        "og:description": "How to easily translate or change the localization identifier with the help of TypoScript."
                    }
                },
                {
                    "title": "Can I deactivate the TYPO3-Cache only for the AMP view?",
                    "markdown": "Faq/Typo3cache.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to disable AMP view cache using TypoScript.",
                        "abstract": "Disable cache for AMP view",
                        "keywords": "TYPO3, extension, AMP, cache, disable, typoscript",
                        "og:title": "TYPO3 AMP - Can I deactivate the TYPO3-Cache only for the AMP view?",
                        "og:description": "How to disable AMP view cache using TypoScript."
                    }
                },
                {
                    "title": "How can I use Web-Fonts in AMP-Views?",
                    "markdown": "Faq/WebFonts.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - How to set up and use the web fonts in your extension HTML template with the help of TypoScript.",
                        "abstract": "How to use web fonts in your extension",
                        "keywords": "TYPO3, extension, AMP, webfonts, fonts, typoscript",
                        "og:title": "TYPO3 AMP - How can I use Web-Fonts in AMP-Views?",
                        "og:description": "How to set up and use the web fonts in your extension HTML template with the help of TypoScript."
                    }
                },
                {
                    "title": "Which TYPO3 versions is this extension compatible with",
                    "markdown": "Faq/CompatibleVersions.en.md",
                    "meta": {
                        "description": "TYPO3 AMP - With which TYPO3 versions this extension is compatible.",
                        "abstract": "Compatibility with TYPO3 versions of TYPO3 AMP",
                        "keywords": "TYPO3, extension, AMP, versions, compatible",
                        "og:title": "TYPO3 AMP - Which TYPO3 versions is this extension compatible with?",
                        "og:description": "With which TYPO3 versions this extension is compatible."
                    }
                }
            ]
        }
    ]
}
