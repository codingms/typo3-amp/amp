# Google Analytics Tracking einfügen

Die erforderlichen Bibliotheken aktivieren:

```typo3_typoscript
plugin.tx_amp {
	settings {
		amp {
			libraries {
				analytics = 1
			}
		}
	}
}
```

Du musst ebenfalls das Google-Analytics-Konto konfigurieren:

```typo3_typoscript
plugin.tx_amp {
	settings {
		tracking {
            googleAnalytics {
                account = UA-XXXXX-Y
            }
        }
	}
}
```

Somit fügt die AMP-Erweiterung die erforderlichen Bibliotheken zu Deiner AMP-Seite hinzu und das Tracking wird beim Seitenaufruf durchgeführt.
