# Insert Google Analytics tracking

Activate the required AMP-Library:

```typo3_typoscript
plugin.tx_amp {
	settings {
		amp {
			libraries {
				analytics = 1
			}
		}
	}
}
```

You also need to configure your Google Analytics Account:

```typo3_typoscript
plugin.tx_amp {
	settings {
		tracking {
            googleAnalytics {
                account = UA-XXXXX-Y
            }
        }
	}
}
```

Finally the AMP extension will add the required libraries to your AMP-Page and call the tracking on *page view*.