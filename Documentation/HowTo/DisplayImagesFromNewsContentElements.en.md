# Show News Images with Content Elements

News images with content elements can be displayed as follows:

1.  Install the [VHS](https://extensions.typo3.org/extension/vhs "VHS Viewhleper Download") extension.

2.  Edit the Fluid-Partial that displays news ( `Partials/Library/News.html` ) and add the VHS namespace:

    ```xml
    <div xmlns="http://www.w3.org/1999/xhtml" lang="en"
         xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
         xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers"
         xmlns:vhs="http://typo3.org/ns/FluidTYPO3/Vhs/ViewHelpers">
         ...
    </div>
    ```
3.  Read in the image resources

    ```xml
    <vhs:resource.record.fal table="tt_content" field="image" uid="{contentElement.uid}" as="resources">
        <f:if condition="{f:count(subject: resources)}">
            <div class="images">
                <f:for each="{resources}" as="resource">
                    <amp-img src="{f:uri.image(src: resource.uid, treatIdAsReference: 1, width: settings.news.image.width, height: settings.news.image.height)}"
                             width="{settings.news.image.width}" height="{settings.news.image.height}"
                             layout="responsive" alt="{resource.alt}" title="{resource.title}"></amp-img>
                </f:for>
            </div>
        </f:if>
    </vhs:resource.record.fal>
    ```

    The completed template section should look as follows:

    ```xml
    <f:for each="{data.contentElements}" as="contentElement">
        <div class="element">
            <f:if condition="{contentElement.header}">
                <h2>{contentElement.header}</h2>
            </f:if>
            <f:if condition="{contentElement.subheader}">
                <h3>{contentElement.subheader}</h3>
            </f:if>
            <vhs:resource.record.fal table="tt_content" field="image" uid="{contentElement.uid}" as="resources">
                <f:if condition="{f:count(subject: resources)}">
                    <div class="images">
                        <f:for each="{resources}" as="resource">
                            <amp-img src="{f:uri.image(src: resource.uid, treatIdAsReference: 1, width: settings.news.image.width, height: settings.news.image.height)}"
                                     width="{settings.news.image.width}"
                                     height="{settings.news.image.height}"
                                     layout="responsive"
                                     alt="{resource.alt}"
                                     title="{resource.title}"></amp-img>
                        </f:for>
                    </div>
                </f:if>
            </vhs:resource.record.fal>
            <f:format.html>{contentElement.bodytext}</f:format.html>
        </div>
    </f:for>
    ```
