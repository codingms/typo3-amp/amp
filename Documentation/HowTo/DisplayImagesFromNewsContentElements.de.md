# Bilder aus News mit Inhaltselementen anzeigen

Um auch Bilder aus News mit Inhaltselementen anzeigen zu können, gehe wie folgt vor:

1.  Installiere die Erweiterung [VHS](https://extensions.typo3.org/extension/vhs "VHS Viewhleper Download").

2.  Bearbeite das Fluid-Partial für die Anzeige der News ( `Partials/Library/News.html` ) und füge den VHS-Namespace hinzu:

    ```xml
    <div xmlns="http://www.w3.org/1999/xhtml" lang="en"
         xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
         xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers"
         xmlns:vhs="http://typo3.org/ns/FluidTYPO3/Vhs/ViewHelpers">
         ...
    </div>
    ```
3.  Lese die Bild-Resourcen

    ```xml
    <vhs:resource.record.fal table="tt_content" field="image" uid="{contentElement.uid}" as="resources">
        <f:if condition="{f:count(subject: resources)}">
            <div class="images">
                <f:for each="{resources}" as="resource">
                    <amp-img src="{f:uri.image(src: resource.uid, treatIdAsReference: 1, width: settings.news.image.width, height: settings.news.image.height)}"
                             width="{settings.news.image.width}" height="{settings.news.image.height}"
                             layout="responsive" alt="{resource.alt}" title="{resource.title}"></amp-img>
                </f:for>
            </div>
        </f:if>
    </vhs:resource.record.fal>
    ```

	Ein fertiger Template-Abschnitt könnte wie folgt aussehen:

	```xml
	<f:for each="{data.contentElements}" as="contentElement">
		<div class="element">
			<f:if condition="{contentElement.header}">
				<h2>{contentElement.header}</h2>
			</f:if>
			<f:if condition="{contentElement.subheader}">
				<h3>{contentElement.subheader}</h3>
			</f:if>
			<vhs:resource.record.fal table="tt_content" field="image" uid="{contentElement.uid}" as="resources">
				<f:if condition="{f:count(subject: resources)}">
					<div class="images">
						<f:for each="{resources}" as="resource">
							<amp-img src="{f:uri.image(src: resource.uid, treatIdAsReference: 1, width: settings.news.image.width, height: settings.news.image.height)}"
									 width="{settings.news.image.width}"
									 height="{settings.news.image.height}"
									 layout="responsive"
									 alt="{resource.alt}"
									 title="{resource.title}"></amp-img>
						</f:for>
					</div>
				</f:if>
			</vhs:resource.record.fal>
			<f:format.html>{contentElement.bodytext}</f:format.html>
		</div>
	</f:for>
	```
