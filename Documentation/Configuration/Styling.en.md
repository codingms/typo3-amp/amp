# Styling the AMP view
Styling the AMP view works just like normal HTML pages which are styled by using CSS. The AMP-Extension comes with a small Static-Template, which contains some basic styles. To use it just include the Static-Template.

Static-Template styles are contained in a TypoScript-Node:

```typo3_typoscript
plugin.tx_amp {
	settings {
		stylesheets.basic (

			body {
				background-color: white;
				padding: 15px;
			}
			/* ..more styles.. */
		)
	}
}
```

To add additional styles for a specific AMP view, you need to add them to the `additional` TypoScript node as follows:

```typo3_typoscript
plugin.tx_amp {
	settings {
		stylesheets.additional (

			/* Specific styles for the EXT:openimmo */
			body {
				background-color: red;
			}
			/* ..more styles.. */
		)
	}
}
```

Internally the AMP-Extension merges `stylesheets.basic` and `stylesheets.additional` and compresses it, so that it fits well into the AMP view.


>	#### Warning: {.alert .alert-danger}
>
>	AMP doesn't allow all styles! More information on: https://www.ampproject.org/docs/guides/responsive/style_pages
