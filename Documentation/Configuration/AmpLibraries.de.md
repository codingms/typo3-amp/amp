# Unterstützte AMP-Bibliotheken:

*	font (zum Verwenden von Web-Schriften)
*	form (zum Verwenden von Formularen)
*	analytics (zum Verwenden von Google-Analytics)
*	carousel (in der Entwicklung)
*	weitere unterstützte AMP-Bibliotheken auf Anfrage


>	#### Support: {.alert .alert-info}
>
>	Du benötigst weitere individuelle Funktionalitäten? Sprich uns an und wir konzipieren und integrieren diese gern mit Dir. Auf Anfrage erstellen wir Dir auch Accordions, Carousels oder Socialmedia-Integrationen wie z.B. Twitter, Youtube oder Instagram.
