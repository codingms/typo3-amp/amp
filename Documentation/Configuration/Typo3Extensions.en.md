# Supported TYPO3-Extensions:

*	tt_news: News
*	news: Modern new system
*	openimmo: Realty management with OpenImmo interface
*	shop: Simple Shop-Extension for TYPO3
*	more supported TYPO3-Extensions on request


>	#### Support: {.alert .alert-info}
>
>	You need support for a further TYPO3-Extension? Just send us a message and we'll be happy to integrate them with you.

