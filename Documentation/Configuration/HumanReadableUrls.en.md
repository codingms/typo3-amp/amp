# Human readable URLs

## Slug configuration (since TYPO3 9.5)

For the slug configuration you only need to add a *PageTypeSuffix*:

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      amp: 1486816934
      amp-form: 1487847316
```



## Realurl configuration (up to TYPO3 9.5)

The following Realurl configuration prepends `/amp/` to your urls:

```php
'preVars' => [
	0 => [
		'GETvar' => 'type',
		'valueMap' => [
			'amp' => '1486816934',
			],
		'noMatch' => 'bypass',
	],
],
```

Integration into a multi language website could look as follows:

```php
'preVars' => [
	0 => [
        'GETvar' => 'L',
        'valueMap' => [
            'de' => '1',
        ],
        'noMatch' => 'bypass',
	],
	1 => [
		'GETvar' => 'type',
		'valueMap' => [
			'amp' => '1486816934',
        ],
		'noMatch' => 'bypass',
	],
],
```
