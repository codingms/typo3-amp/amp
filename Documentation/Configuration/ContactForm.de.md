# Kontaktformular

Wenn Du die PRO-Version von TYPO3-AMP nutzt, köannst Du ein Kontaktformular zu Deiner AMP Seite hinzufügen

*	Installiere die PRO-Version von TYPO3-AMP
*	Füge das static template `AMP - 4. PRO: Form` hinzu.
*   Stelle sicher, dass Deine Website unter https läuft (http ist nicht ausreichend).

Du musst die erforderlichen AMP Bibliotheken aktivieren:

```typo3_typoscript
plugin.tx_amp {
	settings {
		amp {
			libraries {
				form = 1
				mustache = 1
			}
		}
	}
}
```

Abschließend, musst du das Formular konfigurieren:

```typo3_typoscript
plugin.tx_amp {
	settings {
		amp {
			forms {
				contact {
					key = contact
					enabled = 1
					headline = Kontakt aufnehmen
					messages {
						success = Anfrage wurde erfolgreich versandt. Wir werden uns in Kürze mit Ihnen in Verbindung setzen.
					}
					fields {
						canonical {
							type = Hidden
							section = ValueFromTypoScript
							required = 0
							value = lib.amp.canonicalUrl
							# Label for E-Mailmessage
							label = Article
						}
						article {
							type = Hidden
							section = ValueFromTypoScript
							required = 0
							value = lib.amp.url
							# Label for E-Mailmessage
							label = Article
							ignoreInEmail = 1
						}
						company {
							label = Firma
							placeholder = Mustermann GmbH
							type = Input
							section = Default
							required = 0
							value =
						}
						name {
							label = Name
							placeholder = Name
							type = Input
							section = Default
							required = 1
							validation {
								NotEmpty = Bitte geben Sie Ihren Namen ein
							}
							value =
						}
						email {
							label = E-Mail
							placeholder = E-Mailadresse
							type = Email
							section = Default
							required = 1
							validation {
								NotEmpty = Bitte geben Sie Ihre E-Mailadresse ein
								Email = Bitte geben Sie eine gültige E-Mailadresse ein
							}
							value =
						}
						phone {
							label = Telefon
							placeholder = Telefon
							type = Input
							section = Default
							value =
						}
						message {
							label = Nachricht
							placeholder = Ihr Anliegen
							type = Textarea
							section = Default
							required = 1
							validation {
								NotEmpty = Bitte geben Sie eine Nachricht ein
							}
							value =
						}
						submit {
							label =
							value = Anliegen abschicken
							type = Submit
							section = Default
                            ignoreInEmail = 1
						}
					}
					# Finisher
					finisher {
						mail {
							# Class/Object
							type = Mail
							active = 1
							subject = TYPO3-AMP: Kontaktanfrage
							message {
								header.0 = Dear admin,
								header.1 = this is a contact request.
								header.2 =
								header.3 = Fields:

								footer.0 = ---
								footer.1 = Thanks for you attention
							}
							from {
								name = Website which is using TYPO3-AMP
								email = mail@domain.com
							}
							to {
								0 {
									name = TYPO3-AMP Form (TO)
									email = mail@domain.com
								}
								#1 {
								#	name = TYPO3-AMP Form (TO-2)
								#	email = mail@domain.com
								#}
							}
							cc {
								#0 {
								#	name = TYPO3-AMP Form (CC)
								#	email = mail@domain.com
								#}
							}
							bcc {
								#0 {
								#	name = TYPO3-AMP Form (BCC)
								#	email = mail@domain.com
								#}
							}
						}
					}

				}
			}
		}
	}
}
```
