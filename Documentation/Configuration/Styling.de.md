# Gestaltung der AMP-Ansicht
Das Gestalten der AMP-Ansicht funktioniert wie bei normalen HTML-Seiten mit CSS. Die AMP-Erweiterung liefert ein kleines statisches Template mit, welches einige Grundstile beinhaltet. Um es zu nutzen, kannst du einfach das statische Template in Deinem Template-Datensatz einschließen.

Die statischen Template Stile sind in einem TypoScript-Knoten enthalten:

```typo3_typoscript
plugin.tx_amp {
	settings {
		stylesheets.basic (

			body {
				background-color: white;
				padding: 15px;
			}
			/* ..more styles.. */
		)
	}
}
```

Um zusätzliche Stile für eine spezielle AMP-Ansicht hinzuzufügen, musst Du diese zu dem TypoScript-Knoten `additional` hinzufügen, wie folgt:

```typo3_typoscript
plugin.tx_amp {
	settings {
		stylesheets.additional (

			/* Specific styles for the EXT:openimmo */
			body {
				background-color: red;
			}
			/* ..more styles.. */
		)
	}
}
```

Intern führt die AMP-Extension `stylesheets.basic` und `stylesheets.additional` zusammen und komprimiert sie so, dass sie gut in die AMP-Ansicht passt.


>	#### Warnung: {.alert .alert-danger}
>
>	AMP erlaubt nicht alle Stile! Mehr Informationen findest Du auf: https://www.ampproject.org/docs/guides/responsive/style_pages
