# Sprechende URLs

## Slug-Konfiguration (ab TYPO3 9.5)

Für die Slug-Konfiguration füge einfach das gewünschte *PageTypeSuffix* hinzu:

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      amp: 1486816934
      amp-form: 1487847316
```


## Realurl-Konfiguration (bis TYPO3 9.5)

Die folgende Realurl Konfiguration stellt `/amp/` Ihren Links voran.

```php
'preVars' => [
	0 => [
		'GETvar' => 'type',
		'valueMap' => [
			'amp' => '1486816934',
			],
		'noMatch' => 'bypass',
	],
],
```

Die Integration in einer mehrsprachigen Webseite könnte wie folgt aussehen:

```php
'preVars' => [
	0 => [
        'GETvar' => 'L',
        'valueMap' => [
            'de' => '1',
        ],
        'noMatch' => 'bypass',
	],
	1 => [
		'GETvar' => 'type',
		'valueMap' => [
			'amp' => '1486816934',
        ],
		'noMatch' => 'bypass',
	],
],
```
