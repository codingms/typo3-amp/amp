# Integration

Just insert the related static template on the detail page of your record. For example: Open the *Template-Module*, select the *Detail-Page* in Page-Tree, select the *Info/Modify* view and click the *Click here to create an extension template* button.

![Create-Extension-Template][#image100] {.inline-image rel=enlarge-image}

Afterwards, click on *Edit the whole template record* in order to include the required static templates.

![Extension-Template-Select-Static-Template][#image200] {.inline-image rel=enlarge-image}

>	#### Notice: {.alert .alert-info}
>
>	Now your AMP-Site should already work with our default Fluid Templates. Just append `?type=1486816934` to the Link of your Detail-Page.


[#image100]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/Create-Extension-Template.png "Create-Extension-Template"
[#image200]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/Extension-Template-Select-Static-Template.png "Extension-Template-Select-Static-Template"



## General configuration

Now you need to setup some general things, so that your AMP page delivers valid structured data.

Firstly, you should define the base URL of your website:

```typo3_typoscript
plugin.tx_amp.settings.baseurl = https://www.your-domain.de/
```

Next, define the author and publisher of your AMP page.

```typo3_typoscript
plugin.tx_amp.settings.defaults.author = The author name
plugin.tx_amp.settings.defaults.publisher.name = The publisher name
plugin.tx_amp.settings.defaults.publisher.logo.url = https://www.your-domain.de/typo3conf/ext/theme_bootstrap4_xy/Resources/Public/Images/logo.png
plugin.tx_amp.settings.defaults.publisher.logo.width = 346
plugin.tx_amp.settings.defaults.publisher.logo.height = 107
```

>	#### Info: {.alert .alert-info}
>
>	If your News (or other record) doesn't contain an image, the publisher image will be used.

Finally, test your structured data with Google's testing tool: https://search.google.com/structured-data/testing-tool

