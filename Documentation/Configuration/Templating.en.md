# Templating

The AMP-Extension uses simple Fluid-Templates for rendering the AMP-Page. Normally you will only override the Partial-Rootpath when you create your own templates. Therefore you need to set the Partial-Rootpath TypoScript-Constant to your Partial-Folder:

![TypoScript-Constants-Override-Partial-Rootpath][#image100] {.inline-image rel=enlarge-image}

In the Partial folder you will find a `Header.html` and a `Footer.html`. These two files are used for all AMP views. For example, if you insert your graphical header design into the `Header.html` file, the AMP-Extension will use it for all AMP views on your website (News, OpenImmo, ...). There are Partial files for every extension. The name of each Partial file is the extension-key of the related extension (upper-camel-case). These files contain formatting for the records of the extension and are displayed between the `header.html` and the `Footer.html`.



### Header
The header usually contains the website logo with a link to the base site. The default header looks like this:

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers">
	<f:section name="Default">

		<!-- Header:start -->
		<f:if condition="{settings.header.logo.main.file}">
			<a href="{settings.baseurl}" class="logo">
				<amp-img src="{f:uri.image(
							src: settings.header.logo.main.file,
							width: settings.header.logo.main.width,
							height: settings.header.logo.main.height
							)}"
						 width="{settings.header.logo.main.width}"
						 height="{settings.header.logo.main.height}"
						 class="logo"
						 layout="responsive"
						 alt="{settings.header.logo.main.title}"
						 title="{settings.header.logo.main.title}"></amp-img>
			</a>
		</f:if>
		<hr />
		<!-- Header:end -->

	</f:section>
</div>
```

This header can be configured by the following TypoScript-Constants:

```typo3_typoscript
themes.configuration {
	extension {
		amp {
			header {
				logo {
					file =
					width = 230
					height = 71
					title = TYPO3-AMP - Accelerated mobile pages for TYPO3
					alt = TYPO3-AMP - Accelerated mobile pages for TYPO3
				}
			}
		}
	}
}
```



### Footer
The footer usually contains a copyright notice and a link to the Web-Version of this view. This could look like this:

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers">
	<f:section name="Default">

		<!-- Footer:start -->
		<hr />
		<a href="{f:cObject(typoscriptObjectPath: 'lib.amp.canonicalUrl')}">
			<f:translate key="tx_amp_label.display_web_version"/>
		</a>
		<!-- Footer:end -->

	</f:section>
</div>
```

>	#### Info: {.alert .alert-info}
>
>	Make sure that you're using a valid AMP source - check it easily here: https://validator.ampproject.org/

If you're using the AMP PRO-Version, you can insert a contact form related to the displayed record. Your visitors can use the form to easily create enquiries without leaving the view. An integration could look like this:

```xml
<!-- Footer-Form:start -->
<amp:extension.loaded extensionName="AmpPro">
	<f:if condition="{settings.amp.forms.contact.enabled}">
		<amp:render.template file="EXT:amp_pro/Resources/Private/Templates/Amp/Form.html"
							 variables="{form: settings.amp.forms.contact, settings: settings}"
							 paths="{amp:variable.typoscript(path: 'plugin.tx_amp.view')}" />
	</f:if>
</amp:extension.loaded>
<!-- Footer-Form:end -->
```

>	#### Info: {.alert .alert-info}
>
>	In order to get a smaller source, try using EXT:sourceopt. You will find this extension in TYPO3-TER:
>	https://typo3.org/extensions/repository/view/sourceopt.



[#image100]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/TypoScript-Constants-Override-Partial-Rootpath.png "TypoScript-Constants-Override-Partial-Rootpath"
