# Templating

Die AMP-Erweiterung nutzt einfache Fluid-Templates zum Rendern der AMP-Seiten. Normalerweise brauchst Du den Partial-Rootpath nur überschreiben, wenn Du Deine eigenen Fluid-Templates erstellen willst. Hierfür musst Du die TypoScript-Konstante für den Partial-Rootpath auf Deinem Partial-Ordner setzen:

![TypoScript-Konstanten-Partial-Rootpath-Überschreiben][#image100] {.inline-image rel=enlarge-image}

Im Partial-Order findest Du eine `Header.html` und eine `Footer.html`. Diese beiden Dateien werden in allen AMP-Ansichten verwendet. Wenn Du beispielsweise Dein grafisches Design für den Kopf-Bereich in die Datei `Header.html` einfügst, wird es von der AMP-Erweiterung für alle AMP-Ansichten auf Deiner Website (News, OpenImmo, ...) verwendet. Es gibt auch Partial-Dateien für jede Erweiterung. Der Name jeder Partial-Datei ist der Erweiterungsschlüssel (extension-key) der zugehörigen Erweiterung (upper-camel-case). Diese Dateien enthalten Formatierungen für die Datensätze der Erweiterung und werden zwischen `header.html` und `Footer.html` angezeigt.



### Kopf-Bereich (Header)
Der Kopf-Bereich (Header) beinhaltet normalerweise das Logo der Webseite mit einer Verlinkung zu Startseite. Der Standard Kopf sieht wie folgt aus:

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers">
	<f:section name="Default">

		<!-- Header:start -->
		<f:if condition="{settings.header.logo.main.file}">
			<a href="{settings.baseurl}" class="logo">
				<amp-img src="{f:uri.image(
							src: settings.header.logo.main.file,
							width: settings.header.logo.main.width,
							height: settings.header.logo.main.height
							)}"
						 width="{settings.header.logo.main.width}"
						 height="{settings.header.logo.main.height}"
						 class="logo"
						 layout="responsive"
						 alt="{settings.header.logo.main.title}"
						 title="{settings.header.logo.main.title}"></amp-img>
			</a>
		</f:if>
		<hr />
		<!-- Header:end -->

	</f:section>
</div>
```

Dieser Kopf kann mit den folgenden TypoScript-Konstanten konfiguriert werden:

```typo3_typoscript
themes.configuration {
	extension {
		amp {
			header {
				logo {
					file =
					width = 230
					height = 71
					title = TYPO3-AMP - Accelerated mobile pages for TYPO3
					alt = TYPO3-AMP - Accelerated mobile pages for TYPO3
				}
			}
		}
	}
}
```



### Fußzeile (Footer)
Die Fußzeile beinhaltet normalerweise das Urheberrechte (Copyright Notiz) und einen Link zur Web-Version dieser Anzeige. Dies können so aussehen:

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:amp="http://typo3.org/ns/CodingMs/Amp/ViewHelpers">
	<f:section name="Default">

		<!-- Footer:start -->
		<hr />
		<a href="{f:cObject(typoscriptObjectPath: 'lib.amp.canonicalUrl')}">
			<f:translate key="tx_amp_label.display_web_version"/>
		</a>
		<!-- Footer:end -->

	</f:section>
</div>
```

>	#### Info: {.alert .alert-info}
>
>	Stelle sicher, dass Du einen gültigen AMP-Quellecode verwendest - überprüfe es einfach hier: https://validator.ampproject.org/

Wenn Du die AMP PRO-Version verwendest, kannst Du ein Kontaktformular anzeigen lassen. Deine Besucher können das Formular verwenden, um auf einfache Weise Anfragen zu erstellen, ohne die Ansicht zu verlassen. Eine Integration könnte so aussehen:

```xml
<!-- Footer-Form:start -->
<amp:extension.loaded extensionName="AmpPro">
	<f:if condition="{settings.amp.forms.contact.enabled}">
		<amp:render.template file="EXT:amp_pro/Resources/Private/Templates/Amp/Form.html"
							 variables="{form: settings.amp.forms.contact, settings: settings}"
							 paths="{amp:variable.typoscript(path: 'plugin.tx_amp.view')}" />
	</f:if>
</amp:extension.loaded>
<!-- Footer-Form:end -->
```

>	#### Info: {.alert .alert-info}
>
>	Um einen kleineren, optimierten Quellcode zu erhalten, teste die Erweiterung sourceopt. Du findest die Erweiterung im TYPO3-TER:
>	https://typo3.org/extensions/repository/view/sourceopt.



[#image100]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/TypoScript-Constants-Override-Partial-Rootpath.png "TypoScript-Konstanten-Partial-Rootpath-Überschreiben"
