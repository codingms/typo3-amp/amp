# Unterstützte TYPO3-Erweiterungen:

*	tt_news: News
*	news: Modern new system
*	openimmo: Realty management with OpenImmo interface
*	shop: Simple Shop-Extension for TYPO3
*	more supported TYPO3-Extensions on request



>	#### Support: {.alert .alert-info}
>
>	Du benötigst Support für eine weitere TYPO3-Erweiterung? Sprich uns an und wir konzipieren und integrieren diese gern mit Dir.
