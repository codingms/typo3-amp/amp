# Supported AMP-Libraries:

*	font (for using Web-Fonts)
*	form (for using Mail-Forms)
*	analytics (for tracking by Google-Analytics)
*	carousel (in development)
*	more supported AMP-Libraries on request


>	#### Support: {.alert .alert-info}
>
>	Do you need further individual functionalities? Talk to us and we will be happy to design and integrate them with you. On request we can also create accordions, carousels or social media integrations such as Twitter, Youtube or Instagram.

