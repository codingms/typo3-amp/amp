# Integration

Füge einfach das entsprechende statische Template auf Deiner Detailseite hinzu. Zum Beispiel: Öffne die *Template-Werkzeuge*, wähle die *Detailseite* im Seitenbaum, wähle die *Info/Bearbeiten* Ansicht und klicke auf den Button *Klicken Sie hier, um ein Erweiterungs-Template zu erstellen.*.

![Erweiterungs-Template erstellen][#image100] {.inline-image rel=enlarge-image}

Klicke anschließend auf *Vollständigen Template-Datensatz bearbeiten*, um die erforderlichen statischen Templates einzuschließen.

![Erweiterungs-Template-Auswahl-Statisches-Template][#image200] {.inline-image rel=enlarge-image}

>	#### Notiz: {.alert .alert-info}
>
>	Nun sollte Deine AMP-Seite bereits mit unseren Standard Fluid-Templates laufen. Setze einfach `?type=1486816934` ans Ende Deines Links von der Detailseite.


[#image100]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/Create-Extension-Template.png "Erweiterungs-Template erstellen"
[#image200]: https://www.coding.ms/fileadmin/extensions/amp/current/Documentation/Images/Extension-Template-Select-Static-Template.png "Erweiterungs-Template-Auswahl-Statisches-Templat"



## Allgemeine Konfiguration

Nun musst Du einige allgemeine Einstellungen vornehmen, damit Deine AMP-Seite gültige, strukturierte Daten ausliefert.

Zunächst solltest Du die Basis-URL Deiner Website definieren:

```typo3_typoscript
plugin.tx_amp.settings.baseurl = https://www.ihre-domain.de/
```

Definiere als Nächstes den Autor und Herausgeber Deiner AMP-Seite.

```typo3_typoscript
plugin.tx_amp.settings.defaults.author = Der Autor Name
plugin.tx_amp.settings.defaults.publisher.name = Der Herausgeber Name
plugin.tx_amp.settings.defaults.publisher.logo.url = https://www.ihre-domain.de/typo3conf/ext/theme_bootstrap4_xy/Resources/Public/Images/logo.png
plugin.tx_amp.settings.defaults.publisher.logo.width = 346
plugin.tx_amp.settings.defaults.publisher.logo.height = 107
```

>	#### Info: {.alert .alert-info}
>
>	Wenn Ihre News (oder andere Datensätze) kein Bild beinhalten, wird das Herausgeber-Logo verwendet.

Abschließend kontrolliere Deine strukturierten Daten mit Googles Test-Werkzeug:
 https://search.google.com/structured-data/testing-tool
