# I want to disable the AMP Header-Tag while developing the design, but need the AMP-Link - how can I solve this problem?

Just comment out the AMP-Linkwrap:

```typo3_typoscript
lib.amp.url.wrap = <!--link rel="amphtml" href="{$themes.configuration.baseurl}|"-->
```
