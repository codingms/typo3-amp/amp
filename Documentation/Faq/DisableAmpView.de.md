# Kann ich die AMP-Ansicht deaktivieren?

Ja, Du kannst die AMP-Ansicht mit diesem TypoScript deaktivieren:

```typo3_typoscript
	lib.amp.url.wrap = <!--link rel="amphtml" href="{$themes.configuration.baseurl}|"-->
```
