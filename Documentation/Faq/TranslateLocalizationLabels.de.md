# Wie kann ich die Lokalisierungsbezeichnungen übersetzen/ändern?

Du kannst die Lokalisierungskennungen einfach mit TypoScript übersetzen oder ändern:

```typo3_typoscript
plugin.tx_amp {
  _LOCAL_LANG {
    default {
      tx_amp_label.display_web_version = Web-Version anzeigen
    }
    de {
      tx_amp_label.display_web_version = Web-Version anzeigen
    }
    en {
      tx_amp_label.display_web_version = Display Web-Version
    }
  }
}
```
