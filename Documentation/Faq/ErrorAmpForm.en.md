# I receive an error when sending the AMP-Form - how do I fix it?

If you get the following error:

```
Response must contain the AMP-Access-Control-Allow-Source-Origin header
Form submission failed: Error: Response must contain the AMP-Access-Control-Allow-Source-Origin header _reported_
```

make sure you've set a complete baseurl like this:

```typo3_typoscript
plugin.tx_amp.settings.baseurl = https://www.typo3-openimmo.de/
```
