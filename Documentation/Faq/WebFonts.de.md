# Wie verwende ich Web-Fonts in AMP-Views?

Zunächst musst Du die *font* Library aktivieren. Zusätzlich musst Du das HTML-Tag einfügen, um einen Font in den *headerData* TypoScript-Knoten aufzunehmen. Füge einfach dieses TypoScript in Deine Erweiterungsvorlage auf Deiner Detailseite ein.

```typo3_typoscript
plugin.tx_amp {
  settings {
    amp {
      libraries {
        font = 1
      }
      # Add some header data
      headerData (
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400">
      )
    }
  }
}
```

>  **Beachtung:**
>
>  Nicht jedes CDN (Content Delivery Network) darf Web-Fonts in AMP-Pages einbinden. Weitere Informationen findest Du unter: [www.ampproject.org/docs/guides/responsive/custom_fonts](https://www.ampproject.org/docs/guides/responsive/custom_fonts "Amp project: Custom fonts")
