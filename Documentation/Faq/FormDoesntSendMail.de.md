# Das AMP-Formular sendet keine Mail - was tun?

Die AMP-Formularbibliothek fügt automatisch einen Parameter „__amp_source_origin“ an die AJAX-Abfragezeichenfolge an. Dies führt zu einem 404-Fehler aufgrund eines TYPO3-C-Hash-Problems.

Die einzige, dieses Problem zu lösen, besteht darin, die Install-Tool-Einstellung '[FE][pageNotFoundOnCHashError]' auf false festzulegen.
