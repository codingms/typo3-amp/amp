# Kann ich den TYPO3-Cache nur für die AMP-Ansicht deaktivieren?

Ja, Du kannst den TYPO3-Cache mit diesem TypoScript für die AMP-Ansicht deaktivieren:

```typo3_typoscript
[globalVar = GP:type = 1486816934]
    config.no_cache = 1
    page.config.no_cache = 1
[global]
```
