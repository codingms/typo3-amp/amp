# Wie kann ich die URLs auf HTTPS/SSL umstellen?

Verwende einfach diese TypoScript-Konstante:

```typo3_typoscript
themes.configuration.urlScheme = https
```

>  #### Beachtung: {.alert .alert-info}
>
>  Wenn Du das AMP-Formular verwendest, ist HTTPS/SSL unbedingt erforderlich!
