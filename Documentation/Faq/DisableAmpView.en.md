# Can I disable the AMP view?

Yes, you can disable the AMP view by using this TypoScript:

```typo3_typoscript
	lib.amp.url.wrap = <!--link rel="amphtml" href="{$themes.configuration.baseurl}|"-->
```
