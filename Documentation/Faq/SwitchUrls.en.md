# How can I switch the URLs to HTTPS/SSL?

Simply by using using this TypoScript-Constant:

```typo3_typoscript
themes.configuration.urlScheme = https
```

>  #### Info: {.alert .alert-info}
>
>  If you are using the AMP-Form, HTTPS/SSL is absolutely required!
