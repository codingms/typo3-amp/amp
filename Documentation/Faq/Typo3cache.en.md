# Can I deactivate the TYPO3-Cache only for the AMP view?

Yes, you can deactivate the TYPO3-Cache for the AMP view by using this TypoScript:

```typo3_typoscript
[globalVar = GP:type = 1486816934]
  config.no_cache = 1
  page.config.no_cache = 1
[global]
```
