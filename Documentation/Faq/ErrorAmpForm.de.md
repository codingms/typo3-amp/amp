# Beim Senden des AMP-Formulars erhalte ich eine Fehlermeldung - wie kann ich das beheben?

Du hast folgenden Fehler erhalten:

```
Response must contain the AMP-Access-Control-Allow-Source-Origin header
Form submission failed: Error: Response must contain the AMP-Access-Control-Allow-Source-Origin header _reported_
```

Stelle sicher, dass Du eine baseurl wie diese festgelegt hast:

```typo3_typoscript
plugin.tx_amp.settings.baseurl = https://www.typo3-openimmo.de/
```
