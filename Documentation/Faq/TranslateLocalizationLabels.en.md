# How can I translate/modify the localization labels?

You are able to translate or modify the localization labels simply by TypoScript:

```typo3_typoscript
plugin.tx_amp {
  _LOCAL_LANG {
    default {
      tx_amp_label.display_web_version = Web-Version anzeigen
    }
    de {
      tx_amp_label.display_web_version = Web-Version anzeigen
    }
    en {
      tx_amp_label.display_web_version = Display Web-Version
    }
  }
}
```
