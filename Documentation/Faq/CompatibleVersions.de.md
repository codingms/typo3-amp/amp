# Mit welchen TYPO3-Versionen ist diese Extension kompatibel?

Wir versuchen unsere Erweiterungen immer für die aktuellen TYPO3-LTS Versionen kompatibel zu halten. Genaue Informationen zur Kompatibilität sind auf der Produktseite zu finden.
