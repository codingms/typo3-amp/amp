# The AMP-Form doesn't send a mail - what to do?

The AMP Form-Library appends a parameter '__amp_source_origin' automatically to the AJAX query string. This results in a 404 error because of an TYPO3 c-hash issue.

The only way to solve that problem, is to set Install-Tool setting '[FE][pageNotFoundOnCHashError]' to false.
