# Ich möchte den AMP Header-Tag während der Entwicklung des Designs deaktivieren, benötige aber den AMP-Link. Wie kann ich dieses Problem lösen?

Kommentiere einfach den AMP-Linkwrap aus:

```typo3_typoscript
lib.amp.url.wrap = <!--link rel="amphtml" href="{$themes.configuration.baseurl}|"-->
```
