# How can I use Web-Fonts in AMP-Views?

First of all, you need to enable the *font* Library. You also have to insert the HTML-Tag for including a Font into the *headerData* TypoScript node. Just insert this TypoScript into your Extension-Template on your Detail-Page.

```typo3_typoscript
plugin.tx_amp {
  settings {
    amp {
      libraries {
        font = 1
      }
      # Add some header data
      headerData (
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400">
      )
    }
  }
}
```

>  **Attention:**
>
>  Not every CDN (Content delivery network) is allowed to include Web-Fonts on AMP-Pages. For more information visit: [www.ampproject.org/docs/guides/responsive/custom_fonts](https://www.ampproject.org/docs/guides/responsive/custom_fonts "Amp project: Custom fonts")
