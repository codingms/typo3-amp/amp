# Wieso AMP (Accelerated Mobile Pages) für TYPO3?

## Die AMP Erweiterung bietet beschleunigte mobile Seiten für TYPO3.

Accelerated mobile pages (kurz: AMP für "Beschleunigte mobile Seiten") sind "lade-optimiere" Webseite, speziell entworfen für mobile Geräte wie Smartphones und Tablets.

**Die Vorteile auf einen Blick:**

*   AMP-Seiten werden schneller geladen als klassische Webseiten und machen digitale Inhalte für Ihre Besucher zugänglicher.
*   AMP-Seiten werden von Google auf Mobilgeräten besser gewertet und daher oben in der Suchergebnissen aufgeführt. Du kannst AMP-Seiten in den Google Ergebnissen durch ein kleines Blitz-Symbol neben dem Eintrag erkennen.
*   AMP-Seiten sind bereits in den Google-Suchergebnissen in der "Top-Storys-Liste" geladen.
*   Erstelle maßgeschneiderte AMP-Ansichten Deiner Nachrichten, Blogs, Shops, Referenz- oder Immobilienanzeigen und mache diese direkt Deinen Kunden zugänglich.
*   Hierzu bietet Dir die TYPO3-AMP Basisversion einen zusätzlichen TYPO3-Seitentyp, der die AMP-Seiten generiert.

Lese [hier](https://www.ampproject.org/learn/overview/#video "AMP Projekt - Wie AMP im detail funktioniert.") wie AMP im Detail funktioniert.
