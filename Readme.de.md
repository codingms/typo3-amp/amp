# AMP Erweiterung für TYPO3

Accelerated mobile pages (kurz: AMP für "Beschleunigte mobile Seiten") sind "lade-optimiere" Webseite, speziell entworfen für mobile Geräte wie Smartphones und Tablets.


**Features:**

*   Basis-Funktionalität für AMP-Seiten in TYPO3
*   Flexible Konfiguration mit Hilfe von Setup-TypoScript
*   Einfaches Styling mit Fluid und CSS
*   Optional können die AMP-Seiten mit Hilfe von Google-Analytics getrackt werden
*   AMP-Funktionalität für News-Einträge. Dies können News-Einträge der tt_news-Erweiterung oder auch die news-Erweiterung von Georg Ringer sein
*   AMP-Funktionalität für Immobilien-Exposés, welche mit der TYPO3-OpenImmo Erweiterung bereitgestellt werden
*   AMP-Funktionalität für unsere Shop Erweiterung - alle Produkte können nun auch via AMP abgerufen werden
*   Weitere Seitentypen wie Testimonials, Blogeinträge und vieles mehr können problemlos integriert werden – sprich uns an!

**Pro-Features:**

*   Füge einfach Formulare in Deine AMP-Seiten ein, sodass Deine Besucher zielgerichtete Anfragen stellen können
*   Flexible Konfiguration der Formulare mit Hilfe von Setup-TypoScript

Wenn ein zusätzliches oder individuelle Feature benötigt wird - nehme Kontakt auf!


**Links:**

*   [Amp Dokumentation](https://www.coding.ms/documentation/typo3-amp "AMP Dokumentation")
*   [Amp Bug-Tracker](https://gitlab.com/codingms/typo3-amp/amp/-/issues "AMP Bug-Tracker")
*   [Amp Repository](https://gitlab.com/codingms/typo3-amp/amp "AMP Repository")
*   [TYPO3 AMP Webseite](https://www.typo3-amp.de "TYPO3 AMP Webseite")
*   [TYPO3 AMP Produktdetails](https://www.coding.ms/typo3-extensions/typo3-amp/ "TYPO3 AMP Produktdetails")
*   [AMP Project Website](https://www.ampproject.org/ "AMP Project Website")
*   [AMP By Example](https://amp.dev/documentation/examples/ "AMP By Example")
*   [AMP Validator](https://validator.ampproject.org/ "AMP Validator")
