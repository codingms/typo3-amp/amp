<?php

namespace CodingMs\Amp\ViewHelpers\Render;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

class TemplateViewHelper extends AbstractRenderViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('file', 'string', 'Path to template file, EXT:myext/... paths supported', false);
        $this->registerArgument('variables', 'array', 'Optional array of template variables for rendering', false);
        $this->registerArgument('format', 'string', 'Optional format of the template(s) being rendered', false);
        $this->registerArgument('paths', 'array', 'Optional array of arrays of layout and partial root paths, EXT:mypath/... paths supported', false);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        $file = $this->arguments['file'];
        if (null === $file) {
            $file = $this->renderChildren();
        }
        $file = GeneralUtility::getFileAbsFileName($file);
        $view = $this->getPreparedView();
        $view->setTemplatePathAndFilename($file);
        if (is_array($this->arguments['variables'])) {
            $view->assignMultiple($this->arguments['variables']);
        }
        $format = $this->arguments['format'];
        if (null !== $format) {
            $view->setFormat($format);
        }
        $paths = $this->arguments['paths'];
        if (is_array($paths)) {
            if (isset($paths['layoutRootPaths']) && is_array($paths['layoutRootPaths'])) {
                $layoutRootPaths = $this->processPathsArray($paths['layoutRootPaths']);
                $view->setLayoutRootPaths($layoutRootPaths);
            }
            if (isset($paths['partialRootPaths']) && is_array($paths['partialRootPaths'])) {
                $partialRootPaths = $this->processPathsArray($paths['partialRootPaths']);
                $view->setPartialRootPaths($partialRootPaths);
            }
        }
        return $this->renderView($view);
    }

    /**
     * @param array $paths
     * @return array
     */
    protected function processPathsArray(array $paths)
    {
        $pathsArray = [];
        foreach ($paths as $key => $path) {
            $pathsArray[$key] = (0 === strpos($path, 'EXT:')) ? GeneralUtility::getFileAbsFilename($path) : $path;
        }
        return $pathsArray;
    }
}
