<?php

namespace CodingMs\Amp\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Amp\DataProvider\AbstractDataProvider;
use CodingMs\Amp\Service\CssService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * AmpController
 */
class AmpController extends ActionController
{

    /**
     * @var AbstractDataProvider
     */
    protected $dataProvider;

    /**
     * Render AMP content
     */
    public function renderAction()
    {
        // Get data provider
        $dataProvider = null;
        switch ($this->settings['contentType']) {
            case 'openimmo':
                $dataProvider = $this->objectManager->get('CodingMs\\Amp\\DataProvider\\OpenimmoDataProvider');
                break;
            case 'shop':
                $dataProvider = $this->objectManager->get('CodingMs\\Amp\\DataProvider\\ShopDataProvider');
                break;
            case 'news':
                $dataProvider = $this->objectManager->get('CodingMs\\Amp\\DataProvider\\NewsDataProvider');
                break;
            case 'tt_news':
                $dataProvider = $this->objectManager->get('CodingMs\\Amp\\DataProvider\\TtNewsDataProvider');
                break;
        }
        if ($dataProvider instanceof AbstractDataProvider) {
            if ($dataProvider->initialize($this->settings)) {
                $this->view->assign('title', $dataProvider->getTitle());
                $this->view->assign('json', $dataProvider->getJson());
                $this->view->assign('data', $dataProvider->getData());
            }
        }
        // Compress CSS
        $css = '';
        if (isset($this->settings['stylesheets']['basic'])) {
            $css = $this->settings['stylesheets']['basic'];
        }
        if (isset($this->settings['stylesheets']['additional'])) {
            $css .= $this->settings['stylesheets']['additional'];
        }
        $this->settings['stylesheets'] = CssService::compress($css);
        // Get fluid partial
        $partial = GeneralUtility::underscoredToUpperCamelCase($this->settings['contentType']);
        $this->view->assign('partial', $partial);
        // Settings
        if (!isset($this->settings['header']['logo']['main']['height']) ||
            trim($this->settings['header']['logo']['main']['height']) == '') {
            $this->settings['header']['logo']['main']['height'] = 40;
        }
        $this->settings['header']['logo']['main']['fullPath'] = 0;
        if (substr($this->settings['header']['logo']['main']['file'], 0, 4) === 'http') {
            $this->settings['header']['logo']['main']['fullPath'] = 1;
        }
        $this->view->assign('settings', $this->settings);
    }
}
