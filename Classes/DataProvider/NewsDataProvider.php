<?php

namespace CodingMs\Amp\DataProvider;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use GeorgRinger\News\Domain\Model\News;
use GeorgRinger\News\Domain\Repository\NewsRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * News data provider
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class NewsDataProvider extends AbstractDataProvider
{

    /**
     * @var \GeorgRinger\News\Domain\Model\News
     */
    protected $news;

    /**
     * @var \GeorgRinger\News\Domain\Repository\NewsRepository
     */
    protected $newsRepository;

    /**
     * @param \GeorgRinger\News\Domain\Repository\NewsRepository $newsRepository
     */
    public function injectNewsRepository(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * The canonical URL of the article page.
     * @var string
     * @todo move to abstract class
     */
    protected $mainEntityOfPage = '';

    /**
     * Initialize data provider
     * @param array $settings
     * @return bool
     */
    public function initialize(array $settings=[])
    {
        $success = false;
        $parameter = GeneralUtility::_GP('tx_news_pi1');
        if (is_array($parameter) && isset($parameter['news'])) {
            $newsUid = (int)$parameter['news'];
            $this->news = $this->newsRepository->findByIdentifier($newsUid);
            $success = true;
        }
        if (!($this->news instanceof News)) {
            $GLOBALS['TSFE']->pageNotFoundAndExit('News not found!');
        }
        // Author and Publisher defaults
        $this->author= $settings['defaults']['author'];
        $this->publisher = $settings['defaults']['publisher'];
        // Image
        $images = $this->news->getMedia();
        if (count($images) > 0) {
            /** @var \GeorgRinger\News\Domain\Model\FileReference $image */
            foreach ($images as $image) {
                $temp = [];
                $publicUrl = $image->getOriginalResource()->getPublicUrl();
                $absoluteUrl = GeneralUtility::getFileAbsFileName($publicUrl);
                $temp['url'] = $settings['baseurl'] . $publicUrl;
                $size = getimagesize($absoluteUrl);
                $temp['width'] = (string)$size[0];
                $temp['height'] = (string)$size[1];
                $temp['caption'] = $image->getDescription();
                $temp['@type'] = 'ImageObject';
                $this->images[] = $temp;
            }
        } else {
            $this->images[] = [
                'url' => $settings['defaults']['publisher']['logo']['url'],
                'width' => $settings['defaults']['publisher']['logo']['width'],
                'height' => $settings['defaults']['publisher']['logo']['height'],
                'caption' => $settings['defaults']['publisher']['name'],
                '@type' => 'ImageObject'
            ];
        }
        /**
         * @todo Das Feld mainEntityOfPage ist ein empfohlenes Feld. Bitte geben Sie einen Wert ein, falls verfügbar.
         */
        return $success;
    }

    /**
     * Returns the HTML title for AMP page
     * @return string
     */
    public function getTitle()
    {
        return $this->news->getTitle();
    }

    /**
     * Returns the author
     * @return string
     */
    public function getAuthor()
    {
        $author =$this->news->getAuthor();
        if (trim($author) !== '') {
            $this->author = $author;
        }
        return $this->author;
    }

    /**
     * Returns the JSON for AMP page
     * @return array
     */
    public function getJson()
    {
        $json = [
            '@context' => 'http://schema.org',
            '@type' => 'NewsArticle',
            'headline' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'publisher' => [
                'name' => $this->publisher['name'],
                '@type' => $this->publisher['type'],
                'logo' => [
                    'url' => $this->publisher['logo']['url'],
                    'width' => $this->publisher['logo']['width'],
                    'height' => $this->publisher['logo']['height'],
                    '@type' => 'ImageObject'
                ]
            ],
            'datePublished' => date(\DateTime::ISO8601, $this->news->getDatetime()->getTimestamp()),
            'dateModified' => date(\DateTime::ISO8601, $this->news->getTstamp()->getTimestamp()),
            'image' => $this->images
        ];
        return json_encode($json, JSON_UNESCAPED_SLASHES);
    }

    /**
     * Returns the Data for AMP page
     * @return mixed
     */
    public function getData()
    {
        return $this->news;
    }
}
