<?php

namespace CodingMs\Amp\DataProvider;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * TT-News data provider
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class TtNewsDataProvider extends AbstractDataProvider
{

    /**
     * @var array
     */
    protected $news;

    /**
     * @var array
     */
    protected $images = [];

    /**
     * The canonical URL of the article page.
     * @var string
     * @todo move to abstract class
     */
    protected $mainEntityOfPage = '';

    /**
     * Initialize data provider
     * @param array $settings
     * @return bool
     */
    public function initialize(array $settings=[])
    {
        $success = false;
        $parameter = GeneralUtility::_GP('tx_ttnews');
        if (is_array($parameter) && isset($parameter['tt_news'])) {
            $newsUid = (int)$parameter['tt_news'];
            $this->news = BackendUtility::getRecord('tt_news', $newsUid);
            // Explode images
            $this->news['image'] = GeneralUtility::trimExplode(',', $this->news['image'], true);
            $this->news['imagecaption'] = GeneralUtility::trimExplode(',', $this->news['imagecaption'], true);
            $this->news['imagealttext'] = GeneralUtility::trimExplode(',', $this->news['imagealttext'], true);
            $this->news['imagetitletext'] = GeneralUtility::trimExplode(',', $this->news['imagetitletext'], true);
            if (!empty($this->news['image'])) {
                foreach ($this->news['image'] as $imageKey => $image) {
                    $temp = [];
                    $absoluteUrl = GeneralUtility::getFileAbsFileName('uploads/pics/' . $image);
                    $temp['url'] = $settings['baseurl'] . 'uploads/pics/' . $image;
                    // Image size
                    $size = getimagesize($absoluteUrl);
                    $temp['width'] = $size[0];
                    $temp['height'] = $size[1];
                    // Image caption
                    $temp['caption'] = '';
                    if (isset($this->news['imagecaption'][$imageKey])) {
                        $temp['caption'] = $this->news['imagecaption'][$imageKey];
                    }
                    // Image alternative text
                    //$temp['alt'] = $this->news['title'];
                    //if (isset($this->news['imagealttext'][$imageKey])) {
                    //    $temp['alt'] = $this->news['imagealttext'][$imageKey];
                    //}
                    // Image title text
                    //$temp['title'] = $this->news['title'];
                    //if (isset($this->news['imagetitletext'][$imageKey])) {
                    //    $temp['title'] = $this->news['imagetitletext'][$imageKey];
                    //}
                    $temp['@type'] = 'ImageObject';
                    $this->images[] = $temp;
                }
            }
            // Author, set default author if empty
            if (trim($this->news['author']) === '') {
                $this->news['author'] = $settings['defaults']['author'];
            }
            // Publisher, set default author if empty
            $this->publisher = $settings['defaults']['publisher'];
            /**
             * @todo Das Feld mainEntityOfPage ist ein empfohlenes Feld. Bitte geben Sie einen Wert ein, falls verfügbar.
             */
            //$this->mainEntityOfPage = $settings[]
            $success = true;
        }
        return $success;
    }

    /**
     * Returns the HTML title for AMP page
     * @return string
     */
    public function getTitle()
    {
        return $this->news['title'];
    }

    /**
     * Returns the JSON for AMP page
     * @return array
     */
    public function getJson()
    {
        $json = [
            '@context' => 'http://schema.org',
            '@type' => 'NewsArticle',
            'headline' => $this->news['title'],
            'author' => $this->news['author'],
            'publisher' => [
                'name' => $this->news['author'],
                '@type' => $this->publisher['type'],
                'logo' => [
                    'url' => $this->publisher['logo']['url'],
                    'width' => $this->publisher['logo']['width'],
                    'height' => $this->publisher['logo']['height'],
                    '@type' => 'ImageObject'
                ]
            ],
            'datePublished' => date(\DateTime::ISO8601, $this->news['datetime']),
            'dateModified' => date(\DateTime::ISO8601, $this->news['tstamp']),
            'image' => $this->images
        ];
        return json_encode($json, JSON_UNESCAPED_SLASHES);
    }

    /**
     * Returns the Data for AMP page
     * @return mixed
     */
    public function getData()
    {
        return $this->news;
    }
}
