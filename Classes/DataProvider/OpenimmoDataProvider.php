<?php

namespace CodingMs\Amp\DataProvider;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Openimmo\Domain\Model\Immobilie;
use CodingMs\Openimmo\Domain\Repository\ImmobilieRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * OpenImmo data provider
 *
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class OpenimmoDataProvider extends AbstractDataProvider
{
    /**
     * @var \CodingMs\Openimmo\Domain\Model\Immobilie
     */
    protected $immobilie;

    /**
     * @var \CodingMs\Openimmo\Domain\Repository\ImmobilieRepository
     */
    protected $immobilieRepository;

    /**
     * @param \CodingMs\Openimmo\Domain\Repository\ImmobilieRepository $immobilieRepository
     */
    public function injectImmobilieRepository(ImmobilieRepository $immobilieRepository)
    {
        $this->immobilieRepository = $immobilieRepository;
    }

    /**
     * @param array $settings
     * @return bool
     */
    public function initialize(array $settings=[])
    {
        $success = false;
        $parameter = GeneralUtility::_GP('tx_openimmo_immobilie');
        if (is_array($parameter) && isset($parameter['immobilie'])) {
            $immobilieUid = (int)$parameter['immobilie'];
            $this->immobilie = $this->immobilieRepository->findByIdentifier($immobilieUid);
            // Author and Publisher defaults
            $this->author= $settings['defaults']['author'];
            $this->publisher = $settings['defaults']['publisher'];
            $success = true;
        }
        if (!($this->immobilie instanceof Immobilie)) {
            $GLOBALS['TSFE']->pageNotFoundAndExit('Property not found!');
        }
        return $success;
    }

    /**
     * Returns the HTML title for AMP page
     * @return string
     */
    public function getTitle()
    {
        return $this->immobilie->getObjekttitel();
    }

    /**
     * Returns the JSON for AMP page
     * @return string
     */
    public function getJson()
    {
        $datePublished = $this->immobilie->getStandVom();
        if (is_string($datePublished)) {
            $datePublished = date(\DateTime::ISO8601, $this->immobilie->getStandVom());
        } else {
            $datePublished = $datePublished->format(\DateTime::ISO8601);
        }
        $json = [
            '@context' => 'http://schema.org',
            '@type' => 'NewsArticle',
            'headline' => $this->immobilie->getObjekttitel(),
            'datePublished' => $datePublished,
            'image' => $this->immobilie->getTitleImage(),
            'author' => $this->author,
            'publisher' => [
                'name' => $this->publisher['name'],
                '@type' => $this->publisher['type'],
                'logo' => [
                    'url' => $this->publisher['logo']['url'],
                    'width' => $this->publisher['logo']['width'],
                    'height' => $this->publisher['logo']['height'],
                    '@type' => 'ImageObject'
                ]
            ],
        ];
        return json_encode($json, JSON_UNESCAPED_SLASHES);
    }

    /**
     * Returns the Data for AMP page
     * @return mixed
     */
    public function getData()
    {
        return $this->immobilie;
    }
}
