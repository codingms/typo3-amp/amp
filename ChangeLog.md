# AMP Change-Log

## 2023-09-xx  Release of version 2.0.x

*	[BUGFIX] Fix translation for documentation title



## 2023-09-08  Release of version 2.0.5

*	[TASK] Optimize documentation
*	[TASK] Migrate ext_tables and ext_localconf



## 2022-09-29 Release of version 2.0.4

*	[TASK] Optimize documentation metadata



## 2022-05-12 Release of version 2.0.3

*	[TASK] Add more documentation for the AMP contact form
*	[TASK] Optimize TypoScript conditions



## 2022-04-04 Release of version 2.0.2

*	[TASK] Release extensions as free download



## 2022-02-22  Release of version 2.0.1

*	[TASK] Add additional form documentation information



## 2022-02-16  Release of version 2.0.0

*	[BUGFIX] Fix initial configuration for contact form
*	[TASK] Add Services.yaml
*	[TASK] Remove object manager definition in Amp controller
*	[TASK] Optimize code style
*	[TASK] Add and normalized some general configuration files
*	[TASK] Rename all TypoScript file extension to .typoscript
*	[TASK] Add and normalized some general configuration files
*	[TASK] Rise PHP version to 7.4
*	[TASK] Migration for TYPO3 11.5 - remove support for TYPO3 9.5
*	[TASK] Add documentations configuration



## 2021-05-21  Release of version 1.6.8

*	[BUGFIX] Fix link in documentation
*	[TASK] Add extension name in composer.json



## 2020-06-18  Release of version 1.6.7

*	[TASK] Add fallback for header image path



## Version 1.6.6

*	[DOC] Add note in the documentation that https is necessary for the form feature
*	[TASK] Add tags to composer.json



## Version 1.6.5

*	[TASK] TYPO3 10 migration: Migrate ViewHelper Namespaces
*	[TASK] TYPO3 10 migration: replace obsolete $_EXTKEY variable
*	[TASK] TYPO3 10 migration: replace inject annotation with inject method
*	[TASK] Add author and publisher to openimmo amp pages



## Release of version 1.6.4

*	[TASK] Updating AMP carousel to version 0.2



## Release of version 1.6.3

*	[TASK] Add condition in News partial for displaying content elements.
*	[TASK] Add how-to section for using images from news with content elements.



## Release of version 1.6.2

*	[BUGFIX] Extend TypoScript conditions in order to avoid warnings.
*	[TASK] Remove page TypoScript include.



## Release of version 1.6.1

*	[TASK] Change Gitlab-CI configuration.
*	[TASK] Working on documentation.
*	[TASK] Working on documentation.
*	[BUGFIX] Fix usage of date in Openimmo data provider.



## Release of version 1.6.0

*	[FEATURE] Add 404 handling.



## Release of version 1.5.1

*	[BUGFIX] Fix TypoScript conditions.
*	[TASK] Add Gitlab-CI configuration.



## Release of version 1.5.0

*	[TASK] Migrating to TYPO3 9.5 only.
*	[TASK] Migrating of TypoScript conditions to symfony expression language.



## Release of version 1.4.0

*	[TASK] Rising PHP version up to minimum 7.1.
*	[TASK] Migrating to TYPO3 9.5.
*	[TASK] Updating AMP mustache to version 0.2



## Release of version 1.3.3

*	[BUGFIX] Fixing some TypoScript assignment issues.
*	[FEATURE] In case of no record images available, we're using the publisher image.
*	[TASK] Optimizing shop template



## Release of version 1.3.2

*	[BUGFIX] Fixing News image in structured data



## Release of version 1.3.0

*	[BUGFIX] Fixing News image
*	[FEATURE] Adding Shop extension support



## Release of version 1.2.0

*	[FEATURE] Adding Google-Analytics Tracking



## Release of version 1.1.1

*	[BUGFIX] Fixing license information
*	[TASK] Migration for TYPO3 8.7



## Release of version 1.0.0

*	[BUGFIX] Fix usage of base url
*	[TASK] Initial development
