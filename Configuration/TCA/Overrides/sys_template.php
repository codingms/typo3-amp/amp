<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Constants', 'AMP - 0. Basics for non THEMES user');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript', 'AMP - 1. Base');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Stylesheets', 'AMP - 2. Stylesheets');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Library/TtNews', 'AMP - 3. News (EXT:tt_news)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Library/News', 'AMP - 3. News (EXT:news)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Library/Openimmo', 'AMP - 3. OpenImmo (EXT:openimmo)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('amp', 'Configuration/TypoScript/Library/Shop', 'AMP - 3. Shop (EXT:shop)');


