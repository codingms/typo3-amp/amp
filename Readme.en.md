# AMP Extension for TYPO3

Accelerated mobile pages (short: AMP) are "load-optimized" webpages, specifically designed for mobile devices like smartphones or tablets.


**Features:**

*   Basic functionality for AMP pages in TYPO3
*   Flexible configuration with Setup-TypoScript
*   Easy styling with Fluid and CSS
*   Option to track AMP pages using Google Analytics
*   AMP functionality for news posts, e.g. from the tt_news or Georg Ringer extensions
*   AMP functionality for property portfolios displayed in the TYPO3 OpenImmo extension
*   AMP functionality for our shop extension - all products can now be accessed via AMP
*   Other page types such as testimonials, blog entries and much more can easily be integrated - contact us!

**Pro-Features:**

*   Easily insert forms into your AMP pages so that your visitors can make targeted inquiries
*   Flexible configuration of the forms using Setup-TypoScript

If you need some additional or custom feature - get in contact!


**Links:**

*   [Amp Documentation](https://www.coding.ms/documentation/typo3-amp "AMP Documentation")
*   [Amp Bug-Tracker](https://gitlab.com/codingms/typo3-amp/amp/-/issues "AMP Bug-Tracker")
*   [Amp Repository](https://gitlab.com/codingms/typo3-amp/amp "AMP Repository")
*   [TYPO3 AMP Website](https://www.typo3-amp.de "TYPO3 AMP Website")
*   [TYPO3 AMP Productdetails](https://www.coding.ms/typo3-extensions/typo3-amp/ "TYPO3 AMP Productdetails")
*   [AMP Project Website](https://www.ampproject.org/ "AMP Project Website")
*   [AMP By Example](https://amp.dev/documentation/examples/ "AMP By Example")
*   [AMP Validator](https://validator.ampproject.org/ "AMP Validator")
